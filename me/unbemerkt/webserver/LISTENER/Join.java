package me.unbemerkt.webserver.LISTENER;

import me.unbemerkt.webserver.Main;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class Join implements Listener {

    @EventHandler
    public void onJoin(PlayerLoginEvent e) {
        int maxplayers = Main.getInstance().f.getInt(".MaxPlayers.Amount"); //replace this with max players
        if ((Bukkit.getOnlinePlayers().size() < maxplayers)  && (e.getResult() == PlayerLoginEvent.Result.KICK_FULL)) {
            e.allow();
        }
    }

}
