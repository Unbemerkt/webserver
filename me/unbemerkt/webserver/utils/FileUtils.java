package me.unbemerkt.webserver.utils;

import me.unbemerkt.webserver.Main;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.BufferedReader;
import java.io.FileReader;

public class FileUtils
{
    public static String getFileContens(String filename)
    {
        FileConfiguration config = Main.getInstance().getConfig();
        String nofoundfile = config.getString(".404file");
        try
        {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(Main.getInstance().getDataFolder() + "//webpages//" + filename));
            StringBuilder stringBuilder = new StringBuilder();
            String line = bufferedReader.readLine();
            while (line != null)
            {
                stringBuilder.append(line);
                stringBuilder.append(System.lineSeparator());
                line = bufferedReader.readLine();
            }
            return stringBuilder.toString();
        }
        catch (Exception e) {

        }

        return getFileContens(nofoundfile);
    }

    public static String getlogContents()
    {
        try
        {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("logs//latest.log"));
            String end = "";
            String line = bufferedReader.readLine();
            while (line != null)
            {
                end = line + "<br>" + end;
                line = bufferedReader.readLine();
            }
            return end;
        }
        catch (Exception e)
        {

        }
        return "<h1>Error</h1>";
    }
}
