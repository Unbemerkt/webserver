package me.unbemerkt.webserver.utils;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

public class FileBuilderV1 {
    private File f;
    private YamlConfiguration c;


    public FileBuilderV1(String FilePath, String FileName) {
        this.f = new File(FilePath, FileName);
        this.c = YamlConfiguration.loadConfiguration(this.f);
    }

    public FileBuilderV1 setValue(String ValuePath, Object Value){
        c.set(ValuePath, Value);
        return this;
    }

    public int getInt(String ValuePath){
        return c.getInt(ValuePath);
    }

    public String getString(String ValuePath){
        return c.getString(ValuePath);
    }
    public boolean exist(){
        return f.exists();
    }
    public void delete(){
        f.delete();
    }
    public long getLong(String ValuePath){
        return c.getLong(ValuePath);
    }
    public boolean getBoolean(String ValuePath){
        return c.getBoolean(ValuePath);
    }
    public Object getObject(String ValuePath){
        return c.get(ValuePath);
    }
    public List<String> getStringList(String ValuePath){
        return c.getStringList(ValuePath);
    }
    public Set<String> getKeys(boolean deep){
        return c.getKeys(deep);
    }

    public ConfigurationSection getConfigorationSection(String Section){
        return c.getConfigurationSection(Section);
    }

    public FileBuilderV1 save(){
        try {
            this.c.save(this.f);
        } catch (IOException e) {
        }
        return this;
    }
}
