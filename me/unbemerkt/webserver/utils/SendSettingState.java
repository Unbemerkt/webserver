package me.unbemerkt.webserver.utils;

public enum SendSettingState {

    OK(), ERR(),NULL(),DISABLEMUDULE(),ENABLEMUDULE();
    public static SendSettingState currentstate;

    public static SendSettingState getSettingState(){
        return SendSettingState.currentstate;
    }
    public static boolean isState(SendSettingState state){
        return SendSettingState.currentstate == state;
    }
    public static void setState(SendSettingState state){
        SendSettingState.currentstate = state;
    }

}
