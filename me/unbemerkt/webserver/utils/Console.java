package me.unbemerkt.webserver.utils;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;

import me.unbemerkt.webserver.Main;
import net.minecraft.server.v1_8_R3.MinecraftServer;
import org.bukkit.Bukkit;


public class Console
        implements HttpHandler
{
    public static String getLog()
    {
        return FileUtils.getlogContents();
    }

    public static void sendCommand(String command)
    {
        Bukkit.getScheduler().runTask(Main.getInstance(), new Runnable()
        {
            public void run()
            {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
            }
        });
    }

    public void handle(HttpExchange httpExchange)
            throws IOException
    {
        if (httpExchange.getRequestURI().getPath().toString().equalsIgnoreCase("/console_send")) {
            sendCommand(httpExchange.getRequestURI().getQuery().toString().replace("%20"," "));
        } else if (httpExchange.getRequestURI().getPath().toString().equalsIgnoreCase("/console_stop")) {
            sendCommand("stop");
        }else if (httpExchange.getRequestURI().getPath().toString().equalsIgnoreCase("/console_rl")) {
            sendCommand("rl");
        }else if (httpExchange.getRequestURI().getPath().toString().equalsIgnoreCase("/setting_send_motd")) {
            System.out.println("Eine neue MOTD wurde gesetzt!");
            Main.plugin.f.setValue(".MOTD.Message",httpExchange.getRequestURI().getQuery().toString().replace("%20"," "));
            Main.plugin.f.save();
            if (Main.plugin.f.getBoolean(".MOTD.use")){
                MinecraftServer.getServer().setMotd(Main.plugin.f.getString(".MOTD.Message").replace("&","§"));
            }else{
                MinecraftServer.getServer().setMotd(Main.plugin.f.getString(".MOTD.Fallback").replace("&","§"));
            }
            SendSettingState.setState(SendSettingState.OK);
        }else if (httpExchange.getRequestURI().getPath().toString().equalsIgnoreCase("/setting_disable_module")) {
            if (httpExchange.getRequestURI().getQuery().toString().replace("%20"," ").equalsIgnoreCase("motd")){
                Main.plugin.f.setValue(".MOTD.use",false);
            }
            Main.plugin.f.save();
            SendSettingState.setState(SendSettingState.DISABLEMUDULE);
        }else if (httpExchange.getRequestURI().getPath().toString().equalsIgnoreCase("/setting_enable_module")) {
            if (httpExchange.getRequestURI().getQuery().toString().replace("%20"," ").equalsIgnoreCase("motd")){
                Main.plugin.f.setValue(".MOTD.use",true);
            }

            Main.plugin.f.save();
            SendSettingState.setState(SendSettingState.ENABLEMUDULE);
        }else if (httpExchange.getRequestURI().getPath().toString().equalsIgnoreCase("/setting_close_state")) {
            SendSettingState.setState(SendSettingState.NULL);
        }
    }
}
