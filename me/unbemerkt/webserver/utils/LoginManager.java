package me.unbemerkt.webserver.utils;

import com.sun.net.httpserver.BasicAuthenticator;
import me.unbemerkt.webserver.Main;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Keno on 19.08.2017.
 */
public class LoginManager {

    BasicAuthenticator basicAuthenticator;


    File f = new File(Main.getInstance().getDataFolder() + "//users.yml");
    YamlConfiguration cfg;

    public LoginManager(){
        cfg = YamlConfiguration.loadConfiguration(f);
        cfg.set("users", new ArrayList<String>());
        saveConfig();


        basicAuthenticator = new BasicAuthenticator("/") {
            @Override
            public boolean checkCredentials(String username, String password) {
                return userExists(username, password);
            }
        };
    }

    public void addUser(String username, String password) {
        List<String> users = cfg.getStringList("users");
        if(!userExists(username)) {
            users.add(username + "---:::---" + password);
            cfg.set("users", users);
            saveConfig();
        }
    }


    public boolean userExists(String username, String password) {
        List<String> users = cfg.getStringList("users");
        for(String str : users) {
            if(str.split("---:::---")[0].equals(username) && str.split("---:::---")[1].equals(password)) {
                return true;
            }
        }

        return false;

    }



    public boolean userExists(String username) {
        List<String> users = cfg.getStringList("users");
        for(String str : users) {
            if(str.split("---:::---")[0].equals(username)) {
                return true;
            }
        }

        return false;

    }

    public void saveConfig(){
        try {
            cfg.save(f);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public BasicAuthenticator getBasicAuthenticator(){
        return basicAuthenticator;
    }

}
