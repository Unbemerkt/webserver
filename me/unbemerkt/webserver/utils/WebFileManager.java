package me.unbemerkt.webserver.utils;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import me.unbemerkt.webserver.Main;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

public class WebFileManager
        implements HttpHandler
{
    FileConfiguration config = Main.getInstance().getConfig();
    String uselogin = config.getString(".Config.UseLogin");
    public WebFileManager()
    {
        load();
    }

    public void load()
    {
        File dir = new File(Main.getInstance().getDataFolder() + "//webpages//");
        if (dir.getParentFile().exists()) {
            dir.getParentFile().mkdirs();
        }
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File[] files = dir.listFiles();
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                if (!files[i].isDirectory()) {
                    if (uselogin.equalsIgnoreCase("true")){
                        Main.getWebServerManager().getServer().createContext("/" + files[i].getName(), this)
                                .setAuthenticator(Main.getLoginManager().getBasicAuthenticator());
                    }else{
                        Main.getWebServerManager().getServer().createContext("/" + files[i].getName(), this);
                    }

                }
            }
        }
        if (uselogin.equalsIgnoreCase("true")){
            Main.getWebServerManager().getServer().createContext("/console_send", new Console())
                    .setAuthenticator(Main.getLoginManager().getBasicAuthenticator());
            Main.getWebServerManager().getServer().createContext("/console_stop", new Console())
                    .setAuthenticator(Main.getLoginManager().getBasicAuthenticator());
            Main.getWebServerManager().getServer().createContext("/console_rl", new Console())
                    .setAuthenticator(Main.getLoginManager().getBasicAuthenticator());

            Main.getWebServerManager().getServer().createContext("/playerinfo", new playerlist())
                    .setAuthenticator(Main.getLoginManager().getBasicAuthenticator());
            Main.getWebServerManager().getServer().createContext("/", this)
                    .setAuthenticator(Main.getLoginManager().getBasicAuthenticator());
            Main.getWebServerManager().getServer().createContext("/setting_send_motd", new Console()).setAuthenticator(Main.getLoginManager().getBasicAuthenticator());
            Main.getWebServerManager().getServer().createContext("/setting_close_state", new Console()).setAuthenticator(Main.getLoginManager().getBasicAuthenticator());
            Main.getWebServerManager().getServer().createContext("/setting_disable_module", new Console()).setAuthenticator(Main.getLoginManager().getBasicAuthenticator());
            Main.getWebServerManager().getServer().createContext("/setting_enable_module", new Console()).setAuthenticator(Main.getLoginManager().getBasicAuthenticator());
        }else{
            Main.getWebServerManager().getServer().createContext("/console_send", new Console());
            Main.getWebServerManager().getServer().createContext("/console_stop", new Console());
            Main.getWebServerManager().getServer().createContext("/console_rl", new Console());
            Main.getWebServerManager().getServer().createContext("/setting_send_motd", new Console());
            Main.getWebServerManager().getServer().createContext("/setting_close_state", new Console());
            Main.getWebServerManager().getServer().createContext("/setting_disable_module", new Console());
            Main.getWebServerManager().getServer().createContext("/setting_enable_module", new Console());

            Main.getWebServerManager().getServer().createContext("/playerinfo", new playerlist());
            Main.getWebServerManager().getServer().createContext("/", this);
        }

    }

    public void handle(HttpExchange httpExchange)
            throws IOException
    {
        String response = FileUtils.getFileContens(httpExchange.getRequestURI().getPath().toString());
        if (SendSettingState.isState(SendSettingState.ERR)){
            response = response.replace("%settings_state%", "<div class='err' role=\"alert\"><p><i class='material-icons' id='close_state'>close</i>  Fehler beim Speichern!</p></div>");
        }else if (SendSettingState.isState(SendSettingState.OK)){
            response = response.replace("%settings_state%", "<div class='ok' role=\"alert\"><p><i class='material-icons' id='close_state'>close</i>  &Auml;nderrung gespeichert!</p></div>");
        }else if (SendSettingState.isState(SendSettingState.DISABLEMUDULE)){
            response = response.replace("%settings_state%", "<div class='err' role=\"alert\"><p><i class='material-icons' id='close_state'>close</i>  Dieses Modul wurde Deaktiviert!</p></div>");
        }else if (SendSettingState.isState(SendSettingState.ENABLEMUDULE)){
            response = response.replace("%settings_state%", "<div class='ok' role=\"alert\"><p><i class='material-icons' id='close_state'>close</i>  Dieses Modul wurde Aktiviert!</p></div>");
        } else if (SendSettingState.isState(SendSettingState.NULL)) {
            response = response.replace("%settings_state%", "");
        }else {
            response = response.replace("%settings_state%", "");
        }
        if (uselogin.equalsIgnoreCase("true")){
            response = response.replace("%console_view%", Console.getLog())
                    .replace("%account%", httpExchange.getPrincipal().getUsername());
        }else{
            response = response.replace("%console_view%", Console.getLog())
                    .replace("%account%", "ERR:LOGIN_NICHT_AKTIV");
        }





        response = response.replace("%player_list%", playerlist.getPlayerlist());

        response = response.replace("%config_use_motd%", Options.getUseMOTD());
        response = response.replace("%config_ist_motd%", Options.getIstMOTD().replace("§","&"));
        response = response.replace("%config_soll_motd%", Options.getSollMOTD().replace("§","&"));
        response = response.replace("%config_maxplayers%", Options.getMaxPlayers()+"");
        response = response.replace("%config_showmaxplayers%", Options.getShowMaxPlayers()+"");
        response = response.replace("%config_ist_showmaxplayers%", Options.getSetShowMaxPlayers()+"");
        response = response.replace("%config_onlineplayers%", Options.getOnlinePlayers()+"");
        httpExchange.sendResponseHeaders(200, response.length());
        OutputStream outputStream = httpExchange.getResponseBody();
        outputStream.write(response.getBytes());
        outputStream.close();
    }
}
