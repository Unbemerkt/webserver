package me.unbemerkt.webserver.utils;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.io.OutputStream;

import me.unbemerkt.webserver.Main;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class playerlist
        implements HttpHandler
{
    public static String getPlayerlist()
    {
        String response = "";
        FileConfiguration config = Main.getInstance().getConfig();
        String nplayersonline = config.getString(".Messages.NoPlayersOnline");
        for (Player all : Bukkit.getOnlinePlayers()) {
            response = response +
                    ("<tr><td><img src=\"https://crafatar.com/renders/body/%playeruuid%\">" +
                            "&nbsp&nbsp</td><td><a href=\"playerinfo?%playername%\"<p>%playername%</a>" +
                            " (UUID: %playeruuid%) <input type=\"button\" class=\"small\" value=\"Kick\" onclick=\"sendAction('kick %playername%')\">" +
                            "<input type=\"button\" class=\"small\" value=\"Ban\" onclick=\"sendAction('ban %playername%')\"> " +
                            "</p></td></tr>").replace("%playername%", all.getName()).replace("%playeruuid%", all.getUniqueId().toString());
        }
        if (response.trim().equalsIgnoreCase("")) {
            response = nplayersonline;
        }
        return response;
    }

    public void handle(HttpExchange httpExchange)
            throws IOException
    {
        String playername = httpExchange.getRequestURI().getQuery().toString();
        if (Bukkit.getPlayer(playername).isOnline())
        {
            Player p = Bukkit.getPlayer(playername);
            String response = FileUtils.getFileContens("hidden/playerinfo");

            response = response
                    .replace("%name%", p.getName())
                    .replace("%uuid%", p.getUniqueId().toString())
                    .replace("%location_world%", p.getLocation().getWorld().getName())
                    .replace("%location_x%", String.valueOf(p.getLocation().getBlockX()))
                    .replace("%location_y%", String.valueOf(p.getLocation().getBlockY()))
                    .replace("%location_z%", String.valueOf(p.getLocation().getBlockZ()))
                    .replace("%health%", String.valueOf(p.getHealth()))
                    .replace("%food%", String.valueOf(p.getFoodLevel()))
                    .replace("%gamemode%", String.valueOf(p.getGameMode()))
                    .replace("%level%", String.valueOf(p.getLevel()))
                    .replace("%ip%", String.valueOf(p.getAddress().getAddress().getHostAddress()));
            httpExchange.sendResponseHeaders(200, response.length());
            OutputStream outputStream = httpExchange.getResponseBody();
            outputStream.write(response.getBytes());
            outputStream.close();
        }
    }
}
