package me.unbemerkt.webserver;

import me.unbemerkt.webserver.LISTENER.Join;
import me.unbemerkt.webserver.utils.*;
import net.minecraft.server.v1_8_R3.MinecraftServer;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main
        extends JavaPlugin
{
    public static Main plugin;
    public FileBuilderV1 f;
    private static Main instance;
    private static String prefix = "§bWebinterface §8| §7";
    private static WebServerManager webServerManager;
    private static LoginManager loginManager;
    int port = getConfig().getInt(".Config.Port");
    String uselogin = getConfig().getString(".Config.UseLogin");
    public void onEnable(){
        ConsoleCommandSender console = Bukkit.getConsoleSender();
        console.sendMessage(prefix+"Erreichbar unter http://localhost:"+port);
        saveDefaultConfig();
        System.out.println(getDataFolder()+"//");
        this.f = new FileBuilderV1(getDataFolder()+"//", "server_options.yml");
        loadServerOptions();
        instance = this;
        plugin = this;
        SendSettingState.setState(SendSettingState.NULL);
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new Join(), this);
        if (f.getString(".MOTD.Message") != null && f.getBoolean(".MOTD.use")){
            MinecraftServer.getServer().setMotd(Main.plugin.f.getString(".MOTD.Message").replace("&","§"));
        }else{
            MinecraftServer.getServer().setMotd(Main.plugin.f.getString(".MOTD.Fallback").replace("&","§"));
        }
        checkMOTD();

        if (uselogin.equalsIgnoreCase("true")){
            loginManager = new LoginManager();
            if (!getConfig().contains(".createdfirstuser")){
                getConfig().set(".createdfirstuser", "false");
                loginManager.addUser("user", "password");
                console.sendMessage(prefix+"Ein neuer Benutzer wurde Erstellt!");
                console.sendMessage(prefix+"Name: user , Password: password");
                getConfig().set(".createdfirstuser","true");
                return;
            }
            if (!getConfig().getString(".createdfirstuser").equalsIgnoreCase("true")){
                loginManager.addUser("user", "password");
                console.sendMessage(prefix+"Ein neuer Benutzer wurde Erstellt!");
                console.sendMessage(prefix+"Name: user , Password: password");
                getConfig().set(".createdfirstuser","true");

            }

        }
        webServerManager = new WebServerManager(port);
        webServerManager.start();

        new WebFileManager();

    }

    private void checkMOTD(){
        if (f.getString(".MOTD.Message") != null && f.getBoolean(".MOTD.use")){
            MinecraftServer.getServer().setMotd(Main.plugin.f.getString(".MOTD.Message").replace("&","§"));
        }else{
            MinecraftServer.getServer().setMotd(Main.plugin.f.getString(".MOTD.Fallback").replace("&","§"));
        }

        Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
            @Override
            public void run() {
                checkMOTD();
            }
        },20*2);
    }

    public void loadServerOptions(){
        if (f.getInt(".FileWasCreated") == 1){
            f.setValue(".FileWasCreated",1);
        }else{
            f.setValue(".FileWasCreated",1);
            f.setValue(".Maintenance",false);
            f.setValue(".MOTD.use",false);
            f.setValue(".MOTD.Message","&7WebServerPlugin by &bUnbemerkt");
            f.setValue(".MOTD.Fallback",getServer().getMotd());
            f.setValue(".MaxPlayers.setShowMaxAmount",false);
            f.setValue(".MaxPlayers.ShowMaxAmount",20);
            f.setValue(".MaxPlayers.Amount",20);
        }
        f.save();
    }

    public void onDisable()
    {
        webServerManager.stop();
    }

    public static Main getInstance()
    {
        return instance;
    }

    public static String getPrefix()
    {
        return prefix;
    }

    public static WebServerManager getWebServerManager()
    {
        return webServerManager;
    }

    public static LoginManager getLoginManager() {
        return loginManager;
    }
}
